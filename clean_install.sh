#!/bin/bash

if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

echo -n "Enter username: "
read CURRENT_USER

if ! id $CURRENT_USER &>/dev/null; then
	echo "User does not exist"
	exit
fi

# Git is  a dependency
# It can be installed with:
# apt install git

if ! dpkg -s git >/dev/null 2>&1; then
	echo "Git is a dependency"
	echo "Installing git"
	apt install git
fi

# Base programs
apt install xorg i3 compton nitrogen fish ranger firefox-esr lightdm curl -y

# Set fish as default shell
/usr/sbin/usermod -s /usr/bin/fish $CURRENT_USER

# Config for base programs
git clone https://gitlab.com/AndrejSusnik/debconfig.git
CONFIG_PATH="/home/$CURRENT_USER/.config"
mv debconfig $CONFIG_PATH 
chown $CURRENT_USER $CONFIG_PATH

git clone https://gitlab.com/AndrejSusnik/walpapers.git
mv walpapers /home/$CURRENT_USER/walpapers

# st setup
git clone https://github.com/LukeSmithxyz/st.git
apt install build-essential fontconfig libx11-dev libxft-dev libharfbuzz-dev -y
make
make install

# NVim setup
apt install neovim 
pip3 install --user neovim
apt install fuse libfuse2 git python3-pip ack-grep -y
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

apt install gnome-control-center htop -y

# Post install
# Open vim and type :PlugInstall
